﻿using System.ComponentModel.DataAnnotations;

namespace McCoy.Models
{
    public class Store
    {
        public int ID { get; set; }
        public string Name { get; set; }
        [Display(Name = "Manager Name")]
        public string ManagerName { get; set; }
        [Display(Name = "Opening Time")]
        public string OpeningTime { get; set; }
        [Display(Name = "Closing Time")]
        public string ClosingTime { get; set; }
    }
}

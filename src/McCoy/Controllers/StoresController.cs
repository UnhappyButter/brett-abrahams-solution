﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using McCoy.Models;

namespace McCoy.Controllers
{
    public class StoresController : Controller
    {

        public IActionResult Index()
        {
            int id;
            List<Store> storeList = System.IO.File.ReadAllLines("C:\\Users\\Brett\\Documents\\Visual Studio 2015\\Projects\\McCoy\\StoreData.csv")
                .Select(x => x.Split(','))
                .Where(x => int.TryParse(x[0], out id))
                .Select(x => new Store
                {
                    ID = Convert.ToInt32(x[0]),
                    Name = x[1],
                    ManagerName = x[2],
                    OpeningTime = x[3],
                    ClosingTime = x[4]
                }).ToList();

            ViewData["Stores"] = storeList;

            return View(storeList);
        }
    }
}

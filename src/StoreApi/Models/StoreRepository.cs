﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StoreApi.Models
{
    public class StoreRepository : IStoreRepository
    {
        public HttpResponseMessage Add(Store store)
        {
            int id;
            List<Store> storeList = File.ReadAllLines("C:\\Users\\Brett\\Documents\\Visual Studio 2015\\Projects\\McCoy\\StoreData.csv")
                .Select(x => x.Split(','))
                .Where(x => int.TryParse(x[0], out id))
                .Select(x => new Store
                {
                    ID = Convert.ToInt32(x[0]),
                    Name = x[1],
                    ManagerName = x[2],
                    OpeningTime = x[3],
                    ClosingTime = x[4]
                }).ToList();

            if (storeList.FirstOrDefault(x => x.ID == store.ID) != null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            storeList.Add(store);

            string[] updatedList = new string[storeList.Count];
            for (int j = 0; j < storeList.Count; j++)
            {
                updatedList[j] = storeList[j].ID.ToString() + ',' + storeList[j].Name + ',' + storeList[j].ManagerName + ',' + storeList[j].OpeningTime + ',' + storeList[j].ClosingTime;
            }

            File.WriteAllLines(@"C:\Users\Brett\Documents\Visual Studio 2015\Projects\McCoy\StoreData.csv", updatedList);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        public HttpResponseMessage Update(Store store)
        {
            int id;
            List<Store> storeList = File.ReadAllLines("C:\\Users\\Brett\\Documents\\Visual Studio 2015\\Projects\\McCoy\\StoreData.csv")
                .Select(x => x.Split(','))
                .Where(x => int.TryParse(x[0], out id))
                .Select(x => new Store
                {
                    ID = Convert.ToInt32(x[0]),
                    Name = x[1],
                    ManagerName = x[2],
                    OpeningTime = x[3],
                    ClosingTime = x[4]
                }).ToList();

            if (storeList.FirstOrDefault(x => x.ID == store.ID) == null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            var i = storeList.FindIndex(x => x.ID == store.ID);
            storeList[i] = store;

            string[] updatedList = new string[storeList.Count];
            for(int j = 0; j < storeList.Count; j++)
            {
                updatedList[j] = storeList[j].ID.ToString() + ',' + storeList[j].Name + ',' + storeList[j].ManagerName + ',' + storeList[j].OpeningTime + ',' + storeList[j].ClosingTime;
            }

            File.WriteAllLines(@"C:\Users\Brett\Documents\Visual Studio 2015\Projects\McCoy\StoreData.csv", updatedList);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Remove(int ID)
        {
            int id;
            List<Store> storeList = File.ReadAllLines("C:\\Users\\Brett\\Documents\\Visual Studio 2015\\Projects\\McCoy\\StoreData.csv")
                .Select(x => x.Split(','))
                .Where(x => int.TryParse(x[0], out id))
                .Select(x => new Store
                {
                    ID = Convert.ToInt32(x[0]),
                    Name = x[1],
                    ManagerName = x[2],
                    OpeningTime = x[3],
                    ClosingTime = x[4]
                }).ToList();

            if (storeList.FirstOrDefault(x => x.ID == ID) == null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            var i = storeList.FindIndex(x => x.ID == ID);
            storeList.RemoveAt(i);

            string[] updatedList = new string[storeList.Count];
            for (int j = 0; j < storeList.Count; j++)
            {
                updatedList[j] = storeList[j].ID.ToString() + ',' + storeList[j].Name + ',' + storeList[j].ManagerName + ',' + storeList[j].OpeningTime + ',' + storeList[j].ClosingTime;
            }

            File.WriteAllLines(@"C:\Users\Brett\Documents\Visual Studio 2015\Projects\McCoy\StoreData.csv", updatedList);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}

﻿using System.Net.Http;

namespace StoreApi.Models
{
    public interface IStoreRepository
    {
        HttpResponseMessage Add(Store store);
        HttpResponseMessage Update(Store store);
        HttpResponseMessage Remove(int ID);
    }
}

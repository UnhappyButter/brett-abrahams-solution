﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using StoreApi.Models;
using System.Net.Http;
using System.Net;

namespace StoreApi.Controllers
{
    [Route("api/[controller]")]
    public class StoreController : Controller
    {
        public StoreController(IStoreRepository storeRepo)
        {
            StoreRepo = storeRepo;
        }
        public IStoreRepository StoreRepo { get; set; }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Add([FromBody] Store store)
        {
            if (store == null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            return StoreRepo.Add(store);
        }

        [HttpPost("{id}")]
        public HttpResponseMessage Update(string id, [FromBody] Store store)
        {
            int storeNumber = 0;
            int.TryParse(id, out storeNumber);

            if (store == null || store.ID != storeNumber)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            return StoreRepo.Update(store);
        }

        [HttpDelete("{id}")]
        public HttpResponseMessage Remove(string id)
        {
            int storeNumber = 0;
            int.TryParse(id, out storeNumber);

            return StoreRepo.Remove(storeNumber);
        }
    }
}